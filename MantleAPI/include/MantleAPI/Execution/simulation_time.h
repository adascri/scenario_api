/*******************************************************************************
* Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  simulation_time.h */
//-----------------------------------------------------------------------------


#ifndef MANTLEAPI_EXECUTION_SIMULATIONTIME_H
#define MANTLEAPI_EXECUTION_SIMULATIONTIME_H

#include <chrono>
#include <cstdint>

namespace mantle_api
{

/// @brief Use milliseconds with std::int64_t representation as simulation time
using SimulationTime = std::chrono::duration<std::int64_t, std::milli>;

}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_SIMULATIONTIME_H

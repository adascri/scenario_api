/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  control_strategy.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_CONTROLSTRATEGY_H
#define MANTLEAPI_TRAFFIC_CONTROLSTRATEGY_H

#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/vector.h>

#include <vector>

namespace mantle_api
{

enum class MovementDomain
{
    kUndefined = 0,
    kLateral,
    kLongitudinal,
    kBoth
};

enum class ControlStrategyType
{
    kUndefined = 0,
    kKeepVelocity,
    kKeepLaneOffset,
    kFollowHeadingSpline,
    kFollowLateralOffsetSpline,
    kFollowVelocitySpline,
    kFollowRoute,
    kAcquireLaneOffset
};

struct ControlStrategy
{
    virtual ~ControlStrategy() = default;

    // TODO: extend by bool use_dynamic_constraints when needed (false assumed at the moment)

    MovementDomain movement_domain{MovementDomain::kUndefined};
    ControlStrategyType type{ControlStrategyType::kUndefined};
};

inline bool operator==(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
    return lhs.movement_domain == rhs.movement_domain && lhs.type == rhs.type;
}

inline bool operator!=(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
    return !(lhs == rhs);
}

struct KeepVelocityControlStrategy : public ControlStrategy
{
    KeepVelocityControlStrategy()
    {
        movement_domain = MovementDomain::kLongitudinal;
        type = ControlStrategyType::kKeepVelocity;
    }
    // Doesn't need configuration attributes. Controller keeps current velocity on adding entity or update
};

struct KeepLaneOffsetControlStrategy : public ControlStrategy
{
    KeepLaneOffsetControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kKeepLaneOffset;
    }
    // Doesn't need configuration attributes. Controller keeps current lane offset on adding entity or update
};

struct FollowHeadingSplineControlStrategy : public ControlStrategy
{
    FollowHeadingSplineControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kFollowHeadingSpline;
    }

    std::vector<mantle_api::SplineSection> heading_splines;
    double default_value{0};
};

struct FollowVelocitySplineControlStrategy : public ControlStrategy
{
    FollowVelocitySplineControlStrategy()
    {
        movement_domain = MovementDomain::kLongitudinal;
        type = ControlStrategyType::kFollowVelocitySpline;
    }

    std::vector<mantle_api::SplineSection> velocity_splines;
    double default_value{0};
};

inline bool operator==(const FollowVelocitySplineControlStrategy& lhs,
                       const FollowVelocitySplineControlStrategy& rhs) noexcept
{
    return lhs.default_value == rhs.default_value && lhs.velocity_splines == rhs.velocity_splines;
}

inline bool operator!=(const FollowVelocitySplineControlStrategy& lhs,
                       const FollowVelocitySplineControlStrategy& rhs) noexcept
{
    return !(lhs == rhs);
}

struct FollowLateralOffsetSplineControlStrategy : public ControlStrategy
{
    FollowLateralOffsetSplineControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kFollowLateralOffsetSpline;
    }

    std::vector<mantle_api::SplineSection> lateral_offset_splines;
};

// TODO: Create new control strategy for following 3D trajectories with shapes NURBS, polyline, clothoid

struct FollowRouteControlStrategy : public ControlStrategy
{
    FollowRouteControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kFollowRoute;
    }

    std::vector<mantle_api::Vec3d> waypoints;
};

enum class Dimension
{
    kUndefined = 0,
    kDistance,
    kRate,
    kTime
};

enum class Shape
{
    kUndefined = 0,
    kStep,
    kCubic,
    kLinear,
    kSinusoidal
};

struct TransitionDynamics
{
    Dimension dimension{Dimension::kUndefined};
    Shape shape{Shape::kUndefined};
    double value{0};
};

struct AcquireLaneOffsetControlStrategy : public ControlStrategy
{
    AcquireLaneOffsetControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kAcquireLaneOffset;
    }

    int road_id{};
    int lane_id{};
    double offset{};
    TransitionDynamics transition_dynamics;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_CONTROLSTRATEGY_H

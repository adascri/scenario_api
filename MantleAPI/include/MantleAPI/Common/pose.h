/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  pose.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_POSE_H
#define MANTLEAPI_COMMON_POSE_H

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>

namespace mantle_api
{
struct Pose
{
    Vec3d position{};
    Orientation3d orientation{};
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_POSE_H

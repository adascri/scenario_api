/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  vector.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_VECTOR_H
#define MANTLEAPI_COMMON_VECTOR_H

#include <MantleAPI/Common/floating_point_helper.h>

#include <cstdint>

namespace mantle_api
{

template <typename T>
struct Vec3
{
    Vec3() = default;
    Vec3(T x, T y, T z) : x{x}, y{y}, z{z} {}

    T x{};
    T y{};
    T z{};

    inline T Length() const { return sqrt((x * x) + (y * y) + (z * z)); }
};

using Vec3d = Vec3<double>;

inline bool operator==(const Vec3d& lhs, const Vec3d& rhs) noexcept
{
    return IsEqual(lhs.x, rhs.x) && IsEqual(lhs.y, rhs.y) && IsEqual(lhs.z, rhs.z);
}

inline bool operator!=(const Vec3d& lhs, const Vec3d& rhs) noexcept
{
    return !(lhs == rhs);
}

inline Vec3d operator-(const Vec3d& lhs, const Vec3d& rhs) noexcept
{
    return {lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z};
}

inline Vec3d operator+(const Vec3d& lhs, const Vec3d& rhs) noexcept
{
    return {lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z};
}

inline Vec3d operator*(const Vec3d& lhs, double d) noexcept
{
    return {lhs.x * d, lhs.y * d, lhs.z * d};
}

inline Vec3d operator*(double d, const Vec3d& rhs) noexcept
{
    return rhs * d;
}

inline Vec3d operator/(const Vec3d& lhs, double d) noexcept
{
    return {lhs.x / d, lhs.y / d, lhs.z / d};
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_VECTOR_H

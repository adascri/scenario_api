/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  bounding_box.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_BOUNDINGBOX_H
#define MANTLEAPI_COMMON_BOUNDINGBOX_H

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/vector.h>

namespace mantle_api
{
/// Bounding box is defined in local entity coordinate system.
/// The origin of the entity coordinate system is defined in relation to the geometric center.
struct BoundingBox
{
    Vec3d geometric_center{0.0, 0.0, 0.0};
    Dimension3d dimension{0.0, 0.0, 0.0};
};

inline bool operator==(const BoundingBox& lhs, const BoundingBox& rhs) noexcept
{
    return lhs.geometric_center == rhs.geometric_center && lhs.dimension == rhs.dimension;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_BOUNDINGBOX_H

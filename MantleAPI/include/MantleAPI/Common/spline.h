
/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  spline.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_SPLINE_H
#define MANTLEAPI_COMMON_SPLINE_H

#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>

#include <array>

namespace mantle_api
{
struct SplineSegment
{
    mantle_api::Vec3d a;
    mantle_api::Vec3d b;
    mantle_api::Vec3d c;
    mantle_api::Vec3d d;
};

struct SplineSection
{
    mantle_api::Time start_time{0};
    mantle_api::Time end_time{0};
    /// @brief Represents the polynomial.
    ///
    /// The array stores in format \f$[a_3, a_2, a_1, a_0]\f$ for a polynomial in form
    /// \f[
    ///   P(x) = \sum_{i=0}^{3} a_{i} x^{i} = a_3 x^3 + a_2 x^2 + a_1 x + a_0
    /// \f]
    std::array<double, 4> polynomial{0, 0, 0, 0};
};

/// @brief Equality comparison for SplineSection.
inline bool operator==(const SplineSection& lhs, const SplineSection& rhs) noexcept
{
    return lhs.start_time == rhs.start_time && lhs.end_time == rhs.end_time &&
           std::equal(lhs.polynomial.begin(),
                      lhs.polynomial.end(),
                      rhs.polynomial.begin(),
                      [](const double a, const double b) { return IsEqual(a, b); });
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_SPLINE_H

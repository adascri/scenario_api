/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  time_utils.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_TIME_UTILS_H
#define MANTLEAPI_COMMON_TIME_UTILS_H

#include <chrono>

namespace mantle_api
{
using Time = std::chrono::duration<std::int64_t, std::milli>;

/// @brief Converts input in [s] to @ref Time.
/// @tparam T Input type, eg. `double`.
/// @param duration Input value
/// @return Duration representing the given input in units of @ref Time.
template <typename T>
inline Time SecondsToTime(T duration)
{
    return std::chrono::duration_cast<Time>(std::chrono::duration<T>{duration});
}

/// @brief Converts input @ref Time to [s].
/// @param time Time
/// @return Duration in seconds representing the passed in @ref Time.
inline double TimeToSeconds(const Time& time)
{
    return static_cast<double>(time.count()) / 1000.0;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_TIME_UTILS_H

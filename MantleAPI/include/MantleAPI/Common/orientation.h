/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  orientation.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_ORIENTATION_H
#define MANTLEAPI_COMMON_ORIENTATION_H

#include <MantleAPI/Common/floating_point_helper.h>

namespace mantle_api
{

template <typename T>
struct Orientation3
{
    Orientation3() = default;
    Orientation3(T yaw, T pitch, T roll) : yaw{yaw}, pitch{pitch}, roll{roll} {}

    T yaw{};
    T pitch{};
    T roll{};
};

using Orientation3d = Orientation3<double>;

inline bool operator==(const Orientation3d& lhs, const Orientation3d& rhs) noexcept
{
    return IsEqual(lhs.yaw, rhs.yaw) && IsEqual(lhs.pitch, rhs.pitch) && IsEqual(lhs.roll, rhs.roll);
}

inline bool operator!=(const Orientation3d& lhs, const Orientation3d& rhs) noexcept
{
    return !(lhs == rhs);
}

inline Orientation3d operator+(const Orientation3d& lhs, const Orientation3d& rhs) noexcept
{
    return Orientation3d{lhs.yaw + rhs.yaw, lhs.pitch + rhs.pitch, lhs.roll + rhs.roll};
}

inline Orientation3d operator-(const Orientation3d& lhs, const Orientation3d& rhs) noexcept
{
    return Orientation3d{lhs.yaw - rhs.yaw, lhs.pitch - rhs.pitch, lhs.roll - rhs.roll};
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_ORIENTATION_H

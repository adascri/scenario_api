/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  dimension.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_DIMENSION_H
#define MANTLEAPI_COMMON_DIMENSION_H

#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/vector.h>

namespace mantle_api
{

template <typename T>
struct Dimension3
{
    T length{};
    T width{};
    T height{};
};

using Dimension3d = Dimension3<double>;

inline bool operator==(const Dimension3d& lhs, const Dimension3d& rhs) noexcept
{
    return IsEqual(lhs.length, rhs.length) && IsEqual(lhs.width, rhs.width) && IsEqual(lhs.height, rhs.height);
}

inline bool operator!=(const Dimension3d& lhs, const Dimension3d& rhs) noexcept
{
    return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_DIMENSION_H

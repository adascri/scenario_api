################################################################################
# Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

cc_library(
    name = "mantle_api",
    hdrs = glob([
        "include/**/*.h",
    ]),
    includes = [
        "include",
    ],
    visibility = [
        "//visibility:public",
    ],
)

cc_library(
    name = "mantle_api_test_utils",
    hdrs = glob([
        "test/**/*.h",
    ]),
    includes = [
        "test",
    ],
    visibility = [
        "//visibility:public",
    ],
    deps = [
        ":mantle_api",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "mantle_api_test",
    timeout = "short",
    srcs = ["test/interface_test.cpp"],
    data = [
        "//Core/Service/Testing:sanitizer-suppressions",
    ],
    tags = ["test"],
    deps = [
        ":mantle_api_test_utils",
    ],
)
